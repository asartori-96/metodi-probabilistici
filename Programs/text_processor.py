import csv
import os
import re  # for regular expressions
import json


class Comment():
    def __init__(self):
        self._hotel_name = ''
        self._author = ''
        self._content = ''
        self._date = ''
        self._no_reader = ''
        self._no_helpful = ''
        self._overall = ''
        self._value = ''
        self._rooms = ''
        self._location = ''
        self._cleanliness = ''
        self._check_in_front_desk = ''
        self._service = ''
        self._business_service = ''
    
    def get_csv_repr(self):
        csv_row = [
            self._hotel_name,
            self._author,
            self._date,
            self._no_reader,
            self._no_helpful,
            self._overall,
            self._value,
            self._rooms,
            self._location,
            self._cleanliness,
            self._check_in_front_desk,
            self._service,
            self._business_service,
            self._content
        ]

        return csv_row


name = "Training"

path_to_training = "../Dataset/"+name+"/"
file_list = os.listdir(path_to_training)
csv_name = "../Dataset/hotel"+name+".csv"
csv_header = [
    "hotel_name", 
    "author", 
    "date", 
    "no_reader",
    "no_helpful",
    "overall",
    "value",
    "rooms",
    "location",
    "cleanliness",
    "check_in_front_desk",
    "service",
    "business_service",
    "content"]

csv_values = {
    "overall": [],
    "value": [],
    "rooms": [],
    "location": [],
    "cleanliness": [],
    "check_in_front_desk": [],
    "service": [],
    "business_service": [],
    "sentiment": ["Very bad", "Bad", "Neutral", "Good", "Very good"]
}

# open on write csv file
with open(csv_name, 'w+', encoding='utf8',newline='') as csvFile:
    writer = csv.writer(csvFile,delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    writer.writerow(csv_header)

    # iter on each file in Training directory, each one is related to an hotel
    for file in file_list:
        hotel_name = file.replace('.dat', '')   # get hotel name from related dat file
        is_first_comment = True

        for line in open(path_to_training + file, 'r', encoding = 'utf8'):
            if line == '\n':    # for new comment or skipping initial header lines
                if not is_first_comment:
                    writer.writerow(curr_comment.get_csv_repr())
                curr_comment = Comment()
                curr_comment._hotel_name = hotel_name
                is_first_comment = False
            
            else:
                r_tag = re.findall("<[^>]*>", line)
                if "<Author>" in r_tag:
                    curr_comment._author = line[len("<Author>"):len(line)].strip('\n')
                if "<Content>" in r_tag:
                    curr_comment._content = line[len("<Content>"):len(line)].strip('\n')
                if "<Date>" in r_tag:
                    curr_comment._date = line[len("<Date>"):len(line)].strip('\n')
                if "<No. Reader>" in r_tag:
                    curr_comment._no_reader = line[len("<No. Reader>"):len(line)].strip('\n')
                if "<No. Helpful>" in r_tag:
                    curr_comment._no_helpful = line[len("<No. Helpful>"):len(line)].strip('\n')
                if "<Overall>" in r_tag:
                    curr_comment._overall = line[len("<Overall>"):len(line)].strip('\n')
                    if curr_comment._overall != '-1' and curr_comment._overall != '0':
                        csv_values["overall"].append(curr_comment._overall)
                if "<Value>" in r_tag:
                    curr_comment._value = line[len("<Value>"):len(line)].strip('\n')
                    if curr_comment._value != '-1':
                        csv_values["value"].append(curr_comment._value)
                if "<Rooms>" in r_tag:
                    curr_comment._rooms = line[len("<Rooms>"):len(line)].strip('\n')
                    if curr_comment._rooms != '-1':
                        csv_values["rooms"].append(curr_comment._rooms)
                if "<Location>" in r_tag:
                    curr_comment._location = line[len("<Location>"):len(line)].strip('\n')
                    if curr_comment._location != '-1':
                        csv_values["location"].append(curr_comment._location)
                if "<Cleanliness>" in r_tag:
                    curr_comment._cleanliness = line[len("<Cleanliness>"):len(line)].strip('\n')
                    if curr_comment._cleanliness != '-1':
                        csv_values["cleanliness"].append(curr_comment._cleanliness)
                if "<Check in / front desk>" in r_tag:
                    curr_comment._check_in_front_desk = line[len("<Check in / front desk>"):len(line)].strip('\n')
                    if curr_comment._check_in_front_desk != '-1':
                        csv_values["check_in_front_desk"].append(curr_comment._check_in_front_desk)
                if "<Service>" in r_tag:
                    curr_comment._service = line[len("<Service>"):len(line)].strip('\n')
                    if curr_comment._service != '-1':
                        csv_values["service"].append(curr_comment._service)
                if "<Business service>" in r_tag:
                    curr_comment._business_service = line[len("<Business service>"):len(line)].strip('\n')
                    if curr_comment._business_service != '-1':
                        csv_values["business_service"].append(curr_comment._business_service)

for data in csv_values:
    csv_values[data] = list(set(csv_values[data]))

# Dump JSON file
file_values = open("../Dataset/metadata_values.json", 'w')
json.dump(csv_values, file_values)

del csvFile