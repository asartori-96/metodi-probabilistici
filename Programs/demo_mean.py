import dash
from dash.dependencies import Input, Output
from dash_table.Format import Format, Scheme, Sign, Symbol
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

df = pd.read_csv('../Dataset/hotelsMeanDemo.csv')
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
 
app.layout = html.Div([
    html.H1('Tripadvisor'),
    html.H5('Progetto Modelli Probalistici 1819'),
    html.H6('Confronto overall reale e overall predetto + Valutazione del rating medio per hotel'),
    dash_table.DataTable(
        id='datatable-interactivity',
        columns=[
            {"name": i, "id": i, "deletable": False, 'type': 'numeric','format': Format(
                precision=3,
                scheme=Scheme.fixed
            ),} for i in df.columns
        ],
        data=df.to_dict('records'),
        editable=False,
        page_size=15,
        filter_action='native',
        filter_query='',
        sort_action='custom',
        sort_mode='multi',
        sort_by=[],
        row_selectable="multi",
        row_deletable=False,
        selected_rows=[]
    ),
    html.Div(id='datatable-interactivity-container')
])


@app.callback(
    Output('datatable-interactivity-container', "children"),
    [Input('datatable-interactivity', "derived_virtual_data"),
     Input('datatable-interactivity', "derived_virtual_selected_rows")])
def update_graphs(rows, derived_virtual_selected_rows):
    if derived_virtual_selected_rows is None:
        derived_virtual_selected_rows = []

    dff = df if rows is None else pd.DataFrame(rows)

    return [
        dcc.Graph(
            id=column,
            figure={
                "data": [
                    {
                        "x": dff["hotel_name"],
                        "y": dff["overall"],
                        "type": "bar",
                        "name": "overall",
                    },
                    {
                        "x": dff["hotel_name"],
                        "y": dff["overall_pred"],
                        "type": "bar",
                        "name": "predicted overall",
                    }
                ],
                "layout": {
                    "xaxis": {"automargin": True},
                    "yaxis": {
                        "automargin": True,
                        "title": {"text": "overall and overall predicted"}
                    },
                    "height": 350,
                    "margin": {"t": 10, "l": 10, "r": 10},
                },
            },
        )
        for column in ["hotel_name"] if column in dff
    ]


if __name__ == '__main__':
    app.run_server(debug=True)