import os

name="Training"
def create_senti_words():
    file_list = os.listdir("input")
    fl_senti_pos = open("output/SentiWords"+name+"-0.txt", "w+", encoding="utf8")
    fl_senti_neg = open("output/SentiWords"+name+"-1.txt", "w+", encoding="utf8")
    fl_word_list = open("../Create_WordList/output/WordList"+name+".txt", "r", encoding="utf8")
    words_list = set(fl_word_list.read().split("\n"))
    i=1
    senti_pos = []
    senti_neg = []
    for fl in file_list:
        if("pos" in fl):
            fl_pos_words = open("input/"+fl, encoding="utf8")
            pos_words = set(fl_pos_words.read().split("\n"))
            for word_l in words_list:
                if (word_l in pos_words):
                    if not (word_l in senti_pos):
                        senti_pos.append(word_l)
        else:
            fl_neg_words = open("input/"+fl, encoding="utf8")
            neg_words = set(fl_neg_words.read().split("\n"))
            for word_l in words_list:
                if (word_l in neg_words):
                    if not (word_l in senti_neg):
                        senti_neg.append(word_l)
        print("File residuals: "+str(len(file_list)-i)+"\n")
        i+=1
    senti_pos.sort()
    senti_neg.sort()
    for w in senti_pos:
        fl_senti_pos.write(w+"\n")
    for w in senti_neg:
        fl_senti_neg.write(w+"\n")
    fl_senti_pos.close()
    fl_senti_neg.close()

create_senti_words()
