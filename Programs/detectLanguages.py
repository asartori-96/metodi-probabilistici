#!! Prime di eseguire andare in scriptWordList.py e rimuovere i check per le stopwords per tutti i linguaggi ed esegui, sennò non trova nulla!
#!! è meglio che esegui lo script wordList e gli fai ritornare la lista con tutte le stwords e la chiami WordListAll.txt. Poi riporti il programma come prima

from nltk.corpus import stopwords

# ----------------------------------------------------------------------
def _calculate_languages_ratios(words):

    languages_ratios = {}


    # Compute per language included in nltk number of unique stopwords appearing in analyzed text
    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)

        languages_ratios[language] = len(common_elements)  # language "score"

    return languages_ratios


# ----------------------------------------------------------------------
def detect_language(text):
    ratios = _calculate_languages_ratios(text)
    sortedElements=sorted(ratios.items(), key=lambda key: key[1],reverse=True)
    # most_rated_language = max(ratios, key=ratios.get)

    return sortedElements


if __name__ == '__main__':
    name="Testing"
    fwords = open("Create_WordList/output/WordList"+name+"DL.txt", 'r')
    words = fwords.read().split("\n")
    language = detect_language(words)
    print(language)