# Integration of sentiment discretized values in hotel comments
# TODO: For code optimization, think about unique main script point, to avoid
# single executions of various different scripts

import csv

remove_negative = False
name = "Testing"
# file containing sentiment values
path_to_sentiment = "asum-original/output/"
# sentiment_file_name = "STO2-T1-S2(2)-A0.1-B0.001,0.1,0.0-G1.0,1.0-I500-Pi-Training.csv"
sentiment_file_name = "STO2-T1-S2(2)-A0.1-B0.001,0.1,0.0-G1.0,1.0-I500-Pi-" + name + ".csv"
# file containing hotels reviews
path_to_reviews = "../Dataset/"
# reviews_file_name = "hotelTraining.csv"
reviews_file_name = "hotel" + name + ".csv"

# new file containing hotel reviews and positive sentiment values
integrated_file_name = "hotel" + name + "Sent.csv"


positive_sentiment_values = []
header = []

with open(path_to_sentiment + sentiment_file_name, 'r', newline='') as csv_sentiment:
    reader = csv.reader(csv_sentiment, delimiter=',', quotechar='"')
    for row in reader:
        positive_sentiment_values.append(float(row[0]))

with open(path_to_reviews + reviews_file_name, 'r', newline='') as csv_reviews:
    reader = csv.reader(csv_reviews)
    header = next(reader, None)
    del header[1]
    del header[1]
    del header[1]
    del header[1]
    del header[9]
    print(header)

with open(path_to_reviews + reviews_file_name, 'r', encoding = 'utf8', newline='') as csv_reviews, open(path_to_reviews + integrated_file_name, 'w+', encoding = 'utf8') as csv_integrated:
    reader = csv.DictReader(csv_reviews, delimiter=',', quotechar='"')
    header_integrated = header + ["sentiment"]  # adding column to positive sentiment value
    writer = csv.DictWriter(csv_integrated, fieldnames=header_integrated, lineterminator='\n', quotechar='"', quoting=csv.QUOTE_ALL)
    writer.writeheader()  # write header in integrated csv file

    line = 0
    #sent 6
    for row in reader:
        if positive_sentiment_values[line] < 0.2:
            row["sentiment"] = "1"
        elif 0.2 <= positive_sentiment_values[line] < 0.4:
            row["sentiment"] = "2"
        elif 0.4 <= positive_sentiment_values[line] < 0.6:
            row["sentiment"] = "3"
        elif 0.6 <= positive_sentiment_values[line] < 0.8:
            row["sentiment"] = "4"
        elif 0.8 <= positive_sentiment_values[line]:
            row["sentiment"] = "5"

    #sent 4
        # for row in reader:
        #     if positive_sentiment_values[line] < 0.34:
        #         row["sentiment"] = "Bad"
        #     elif 0.34 <= positive_sentiment_values[line] < 0.51:
        #         row["sentiment"] = "Passable"
        #     elif 0.51 <= positive_sentiment_values[line] <= 0.68:  # neutral class
        #         row["sentiment"] = "Commendable"
        #     elif 0.68 < positive_sentiment_values[line]:
        #         row["sentiment"] = "Good"
        
        del row["author"]
        del row["date"]
        del row["no_reader"]
        del row["no_helpful"]
        del row["content"]
        if remove_negative:
            if not "-1" in row.values():
                writer.writerow(row)
        else:
            writer.writerow(row)
        line += 1