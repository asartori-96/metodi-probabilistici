library(DataExplorer)
library(corrplot)
library(caret)
library(ggplot2)
library(data.table)
library(rmarkdown)
hotels <- read.csv("../Dataset/hotelTestingSent.csv",header=TRUE,",")
hotels=subset( hotels, select = -1)
# hotels$overall = as.numeric(hotels$overall)
# hotels$value = as.numeric(hotels$value)
# hotels$rooms = as.numeric(hotels$rooms)
# hotels$location = as.numeric(hotels$location)
# hotels$cleanliness = as.numeric(hotels$cleanliness)
# hotels$check_in_front_desk = as.numeric(hotels$check_in_front_desk)
# hotels$service = as.numeric(hotels$service)
# hotels$business_service = as.numeric(hotels$business_service_service)
# hotels$sentiment= as.numeric(hotels$sentiment)
#create_report(hotels)
create_report(
data = hotels,
output_format = html_document(toc = TRUE, toc_depth = 6, theme = "flatly"),
output_file = "report.html",
output_dir = getwd(),
config = configure_report(
  add_plot_density= TRUE,
  global_ggtheme = quote(theme_light())
))