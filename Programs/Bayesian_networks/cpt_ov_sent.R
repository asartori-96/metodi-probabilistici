library(rpart)
library(formattable)
library(pROC)
library(e1071)
library(bnlearn)
library(plyr)
library(visNetwork)
#library(flipAPI)
#leggo csv
hotelsTrain <- read.csv("../../Dataset/hotelTrainingSentNO-1.csv", header = TRUE) 
hotelsTrain$overall = as.factor(hotelsTrain$overall)
hotelsTrain$value = as.factor(hotelsTrain$value)
hotelsTrain$rooms = as.factor(hotelsTrain$rooms)
hotelsTrain$location = as.factor(hotelsTrain$location)
hotelsTrain$cleanliness = as.factor(hotelsTrain$cleanliness)
hotelsTrain$check_in_front_desk = as.factor(hotelsTrain$check_in_front_desk)
hotelsTrain$service = as.factor(hotelsTrain$service)
hotelsTrain$business_service = as.factor(hotelsTrain$business_service)
hotelsTrain$sentiment= as.factor(hotelsTrain$sentiment)
#hotelsTrain$ov=hotelsTrain$overall
#hotelsTrain=rename(hotelsTrain, replace= c("ov"="overall"))
hotelsTrain=subset( hotelsTrain, select = c(2,10))

hotelsTrain <- data.frame(hotelsTrain)


structure <- empty.graph(c("overall", "sentiment"))

modelstring(structure) <- "[overall|sentiment][sentiment]"


plot.network <- function(structure, ht = "400px"){
  nodes.uniq <- unique(c(structure$arcs[,1], structure$arcs[,2]))
  nodes <- data.frame(id = nodes.uniq,
                      label = nodes.uniq,
                      color = "darkorchid",
                      shadow = TRUE)
  
  edges <- data.frame(from = structure$arcs[,1],
                      to = structure$arcs[,2],
                      arrows = "to",
                      smooth = TRUE,
                      shadow = TRUE,
                      color = "black")
  
  return(visNetwork(nodes, edges, height = ht, width = "100%"))
}

# observe structure
print(plot.network(structure))

fittedbn<- bn.fit(structure, data = hotelsTrain)
fittedbn$overall
