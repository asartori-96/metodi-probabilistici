# Import libraries
library(bnlearn)    # for bayesian networks
library(tidyverse)  # to select subset of columns
library(rjson)
library(rpart)
library(formattable)
library(pROC)
library(plyr)
library(visNetwork)
# Set the working directory: TO BE CHANGED ACCORDING TO TERMINAL
#setwd("C:/Users/andre/Desktop/Repo/mpd/metodi-probabilistici")

add_fr_norm <- function(freqs) {
  fr_norm = list()
  
  for(fr in freqs) {
    curr_fr_norm = fr / tot_nrow
    fr_norm = c(fr_norm, curr_fr_norm)
  }
  
  return(fr_norm)
}

# Load training dataset
data = read.csv("Dataset/hotelTrainingSentNO-1.csv", header = TRUE, sep = ",", quote = "\"", allowEscapes = TRUE, encoding = "utf8", stringsAsFactors = FALSE, colClasses = "character")   # short version
inf_data = data
inf_data$value = as.factor(inf_data$value)
inf_data$rooms = as.factor(inf_data$rooms)
inf_data$location = as.factor(inf_data$location)
inf_data$cleanliness = as.factor(inf_data$cleanliness)
inf_data$check_in_front_desk = as.factor(inf_data$check_in_front_desk)
inf_data$service = as.factor(inf_data$service)
inf_data$business_service = as.factor(inf_data$business_service)
inf_data$overall = as.factor(inf_data$overall)
inf_data$sentiment= as.factor(inf_data$sentiment)

names(inf_data)[names(inf_data) == "value"] <- "V"
names(inf_data)[names(inf_data) == "rooms"] <- "R"
names(inf_data)[names(inf_data) == "location"] <- "L"
names(inf_data)[names(inf_data) == "cleanliness"] <- "CL"
names(inf_data)[names(inf_data) == "check_in_front_desk"] <- "CH"
names(inf_data)[names(inf_data) == "service"] <- "SE"
names(inf_data)[names(inf_data) == "business_service"] <- "B"
names(inf_data)[names(inf_data) == "sentiment"] <- "PS"
names(inf_data)[names(inf_data) == "overall"] <- "O"


tot_nrow = nrow(inf_data)                                              # get total row of csv
nodes_values = colnames(inf_data)[1:8]                                 # considers only meta and sentiment

isToArc = FALSE                                                        # for loop below
arc_components = vector()
i = 1

while(i <= length(nodes_values)) {
  if(nodes_values[i] == "O")
    i = i + 1
  if(!isToArc) {
    arc_components = c(arc_components, nodes_values[i])
    isToArc = TRUE
    i = i + 1
  }
  else {
    arc_components = c(arc_components, "O")
    isToArc = FALSE
  }
  if(i == length(nodes_values) + 1)                                     # to add last "overall" as to-arc for "positive_sentiment"
    arc_components = c(arc_components, "O")
}

# Creating arc set
arc.set = matrix(arc_components,
                 ncol = 2, byrow = TRUE,
                 dimnames = list(NULL, c("from", "to")))

# Creating bn object
e = empty.graph(nodes_values)
arcs(e) = arc.set                                                       # network informations
model_net = modelstring(e)                                              # get stringified network model

# FREQUENCIES (to do CPTs)

meta_json <- fromJSON(file = "Dataset/metadata_values.json")

# Value
value_matrix <- matrix(0,ncol=length(meta_json$value))
i = 1
while(i <= length(inf_data$V)){
  position <- strtoi(inf_data$V[i], base = 0L)
  value_matrix[position] = value_matrix[position] + 1
  i = i + 1
}

cpt_value <- c(0,0,0,0,0)
dim(cpt_value) = length(meta_json$value)
dimnames(cpt_value) = list("V"=c(1,2,3,4,5))
i = 1
while(i <= length(value_matrix)){
  cpt_value[i] <- value_matrix[i] / length(inf_data$V)
  i = i + 1
}


# Rooms
rooms_matrix <- matrix(0,ncol=length(meta_json$rooms))
i = 1
while(i <= length(inf_data$R)){
  position <- strtoi(inf_data$R[i], base = 0L)
  rooms_matrix[position] = rooms_matrix[position] + 1
  i = i + 1
}

cpt_rooms <- c(0,0,0,0,0)
dim(cpt_rooms) = length(meta_json$rooms)
dimnames(cpt_rooms) = list("R"=c(1,2,3,4,5))
i = 1
while(i <= length(rooms_matrix)){
  cpt_rooms[i] <- rooms_matrix[i] / length(inf_data$R)
  i = i + 1
}

# Location
location_matrix <- matrix(0,ncol=length(meta_json$location))
i = 1
while(i <= length(inf_data$L)){
  position <- strtoi(inf_data$L[i], base = 0L)
  location_matrix[position] = location_matrix[position] + 1
  i = i + 1
}

cpt_location <- c(0,0,0,0,0)
dim(cpt_location) = length(meta_json$location)
dimnames(cpt_location) = list("L"=c(1,2,3,4,5))
i = 1
while(i <= length(location_matrix)){
  cpt_location[i] <- location_matrix[i] / length(inf_data$L)
  i = i + 1
}

# Cleanliness
cleanliness_matrix <- matrix(0,ncol=length(meta_json$cleanliness))
i = 1
while(i <= length(inf_data$CL)){
  position <- strtoi(inf_data$CL[i], base = 0L)
  cleanliness_matrix[position] = cleanliness_matrix[position] + 1
  i = i + 1
}

cpt_cleanliness <- c(0,0,0,0,0)
dim(cpt_cleanliness) =length(meta_json$cleanliness)
dimnames(cpt_cleanliness) = list("CL"=c(1,2,3,4,5))
i = 1
while(i <= length(cleanliness_matrix)){
  cpt_cleanliness[i] <- cleanliness_matrix[i] / length(inf_data$CL)
  i = i + 1
}

# Check_in_front_desk
check_in_front_desk_matrix <- matrix(0,ncol=length(meta_json$check_in_front_desk))
i = 1
while(i <= length(inf_data$CH)){
  position <- strtoi(inf_data$CH[i], base = 0L)
  check_in_front_desk_matrix[position] = check_in_front_desk_matrix[position] + 1
  i = i + 1
}

cpt_check_in_front_desk <- c(0,0,0,0,0)
dim(cpt_check_in_front_desk) = length(meta_json$check_in_front_desk)
dimnames(cpt_check_in_front_desk) = list("CH"=c(1,2,3,4,5))
i = 1
while(i <= length(check_in_front_desk_matrix)){
  cpt_check_in_front_desk[i] <- check_in_front_desk_matrix[i] / length(inf_data$CH)
  i = i + 1
}

# Service
service_matrix <- matrix(0,ncol=length(meta_json$service))
i = 1
while(i <= length(inf_data$SE)){
  position <- strtoi(inf_data$SE[i], base = 0L)
  service_matrix[position] = service_matrix[position] + 1
  i = i + 1
}

cpt_service <- c(0,0,0,0,0)
dim(cpt_service) = length(meta_json$service)
dimnames(cpt_service) = list("SE"=c(1,2,3,4,5))
i = 1
while(i <= length(service_matrix)){
  cpt_service[i] <- service_matrix[i] / length(inf_data$SE)
  i = i + 1
}

# business_service
business_service_matrix <- matrix(0,ncol=length(meta_json$business_service))
i = 1
while(i <= length(inf_data$B)){
  position <- strtoi(inf_data$B[i], base = 0L)
  business_service_matrix[position] = business_service_matrix[position] + 1
  i = i + 1
}

cpt_business_service <- c(0,0,0,0,0)
dim(cpt_business_service) = length(meta_json$business_service)
dimnames(cpt_business_service) = list("B"=c(1,2,3,4,5))
i = 1
while(i <= length(business_service_matrix)){
  cpt_business_service[i] <- business_service_matrix[i] / length(inf_data$B)
  i = i + 1
}


# Positive sentiment
sentiment_matrix <- matrix(0,ncol=length(meta_json$sentiment))
i = 1
while(i <= length(inf_data$PS)){
  if(inf_data$PS[i] == "1"){
    position = 1
  }
  if(inf_data$PS[i] == "2"){
    position = 2
  }
  if(inf_data$PS[i] == "3"){
    position = 3
  }
  if(inf_data$PS[i] == "4"){
    position = 4
  }
  if(inf_data$PS[i] == "5"){
    position = 5
  }
  
  sentiment_matrix[position] = sentiment_matrix[position] + 1
  i = i + 1
}

cpt_sentiment <- c(0,0,0,0,0)
dim(cpt_sentiment) = length(meta_json$sentiment)
dimnames(cpt_sentiment) = list("PS"=c(1,2,3,4,5))
i = 1
while(i <= length(sentiment_matrix)){
  cpt_sentiment[i] <- sentiment_matrix[i] / length(inf_data$PS)
  i = i + 1
}

# Overall P(Overall | Value, Rooms, Location, Service, Cleanliness, CheckIn, Sent)


#passo 1 - costruisco overall matrix come una matrice di 13 colonne e circa 390625  righe. Dove 8 colonne corrispondono alle 8 variabili che influenzano 
#overall e 5 variabili sono i valori che overall pu� assumere. Siccome overall_matrix � una matrice ausiliare assegno, per comodit�, un valore da 1 a 5 ai 
#sentiment. In questo primo passo riempo le prime 8 colonne assegnando le diverse combinazioni delle variabili

#overall_p = parents(e, "overall")
#print(overall_p)
overall_matrix <- matrix(0,ncol=length(meta_json)-1 + length(meta_json$overall), 
                         nrow = length(meta_json$value)*length(meta_json$rooms)*length(meta_json$location)*length(meta_json$cleanliness)
                         *length(meta_json$check_in_front_desk) *length(meta_json$service)*length(meta_json$business_service)*length(meta_json$sentiment), 
                         dimnames = list(1:390625, c("V","R","L", "CL","CH","SE","B","PS","P(O=1)","P(O=2)","P(O=3)","P(O=4)","P(O=5)")))

a = 1
b = 1
c = 1
d = 1
e = 1
f = 1
g = 1
h = 1
nrow = 1

while (a <= length(meta_json$value)){
  while (b <= length(meta_json$rooms)){
    while (c <= length(meta_json$location)){
      while (d <= length(meta_json$cleanliness)){
        while (e <= length(meta_json$check_in_front_desk)){
          while (f <= length(meta_json$service)){
            while (g <= length(meta_json$business_service)){
              while (h <= length(meta_json$sentiment)){
                overall_matrix[nrow,"PS"]= h
                overall_matrix[nrow,"B"]= g
                overall_matrix[nrow,"SE"]= f
                overall_matrix[nrow,"CH"]= e
                overall_matrix[nrow,"CL"]= d
                overall_matrix[nrow,"L"]= c
                overall_matrix[nrow,"R"]= b
                overall_matrix[nrow,"V"]= a
                nrow = nrow + 1
                h = h + 1
              }
              g = g + 1
              h = 1
              
            }
            f = f + 1
            g = 1
            
          }
          e = e + 1
          f = 1
          
        }
        d = d + 1
        e = 1
        
      }
      c = c + 1
      d = 1
      
    }
    b = b + 1
    c = 1
    
  }
  a = a + 1
  b = 1
  
}


# Passo 2 - Riempo le 5 colonne di overall in base al matching tra inf_data e overall_matrix. nrow(inf_data) - length(inf_data$O)
i = 1
while (i <= length(inf_data$O)){
  pos_value = inf_data[i,"V"]
  pos_rooms = inf_data[i,"R"]
  pos_location = inf_data[i,"L"]
  pos_cleanliness = inf_data[i,"CL"]
  pos_check_in_front_desk = inf_data[i,"CH"]
  pos_service = inf_data[i,"SE"]
  pos_business_service = inf_data[i,"B"]
  pos_sentiment = inf_data[i,"PS"]
  if (pos_sentiment == "1"){
    pos_sentiment = 1
  }
  if (pos_sentiment == "2"){
    pos_sentiment = 2
  }
  if (pos_sentiment == "3"){
    pos_sentiment = 3
  }
  if (pos_sentiment == "4"){
    pos_sentiment = 4
  }
  if (pos_sentiment == "5"){
    pos_sentiment = 5
  }
  val_overall = inf_data[i,"O"]
  
  first_division = (nrow(overall_matrix) /5 ) #value
  second_division = (nrow(overall_matrix) /5 ) / 5 # rooms
  third_division = ((nrow(overall_matrix) /5 ) / 5) / 5 # location
  fourth_division = (((nrow(overall_matrix) /5 ) / 5) / 5 ) / 5 # clean
  fifth_division = ((((nrow(overall_matrix) /5 ) / 5) / 5 ) / 5) / 5 # check_in
  sixth_division = (((((nrow(overall_matrix) /5 ) / 5) / 5 ) / 5) / 5) /5 # service
  seventh_division = ((((((nrow(overall_matrix) /5 ) / 5) / 5 ) / 5) / 5) /5)/5 # business
  eighth_division = (((((((nrow(overall_matrix) /5 ) / 5) / 5 ) / 5) / 5) /5)/5)/5 # sentiment
  
  position = first_division * (as.integer(pos_value) - 1) + second_division * (as.integer(pos_rooms) - 1) + third_division *
    (as.integer(pos_location) - 1) + fourth_division * (as.integer(pos_cleanliness) - 1) + fifth_division * (as.integer(pos_check_in_front_desk) - 1) +
    sixth_division * (as.integer(pos_service) - 1)  + seventh_division* (as.integer(pos_business_service) - 1) +
    eighth_division * (as.integer(pos_sentiment) - 1)  + 1
  
  val_overall = as.numeric(val_overall) + 8 #parto dalla nona colonna
  overall_matrix[position,val_overall] = overall_matrix[position,val_overall] + 1
  
 
  

  print(paste("Sto processando la riga: ", i, " di  ",  nrow(inf_data), " del training set per creare la cpt completa di overall"))
  
  i = i + 1
}

length_overall = nrow(overall_matrix)

#passo 3 - calcolo frequenze assolute e cpt di overall
p = 1
#Calcolo P(O | V, R, B, L, CH, CL, SE, PS) cio� P(A | B) = P(A int B)/P(B)
while (p <= length_overall){
  sum_row = overall_matrix[p,"P(O=1)"] + overall_matrix[p,"P(O=2)"] + overall_matrix[p,"P(O=3)"] + overall_matrix[p,"P(O=4)"] + overall_matrix[p,"P(O=5)"]
  if(sum_row == 0){
    overall_matrix[p,"P(O=1)"] = 0.2
    overall_matrix[p,"P(O=2)"] = 0.2
    overall_matrix[p,"P(O=3)"] = 0.2
    overall_matrix[p,"P(O=4)"] = 0.2
    overall_matrix[p,"P(O=5)"] = 0.2
  }else{
    overall_matrix[p,"P(O=1)"] = overall_matrix[p,"P(O=1)"] / sum_row
    overall_matrix[p,"P(O=2)"] = overall_matrix[p,"P(O=2)"] / sum_row
    overall_matrix[p,"P(O=3)"] = overall_matrix[p,"P(O=3)"] / sum_row
    overall_matrix[p,"P(O=4)"] = overall_matrix[p,"P(O=4)"] / sum_row
    overall_matrix[p,"P(O=5)"] = overall_matrix[p,"P(O=5)"] / sum_row
  }
  p = p + 1
}

save.image(file='data.RData')
#passo 4 - CPT per overall
aux_matrix = overall_matrix[,9:13]

cpt_overall = as.vector(t(aux_matrix))

dim(cpt_overall) = c(length(meta_json$overall),length(meta_json$value),length(meta_json$location), length(meta_json$rooms), length(meta_json$cleanliness),length(meta_json$check_in_front_desk),
                     length(meta_json$service),length(meta_json$business_service),length(meta_json$sentiment))
dimnames(cpt_overall) = list("O" = c(1,2,3,4,5), "V" = c(1,2,3,4,5),"L" = c(1,2,3,4,5),"R" = c(1,2,3,4,5),"CL" = c(1,2,3,4,5) ,
                             "CH" = c(1,2,3,4,5),"SE" = c(1,2,3,4,5),"B" = c(1,2,3,4,5),"PS" = c(1,2,3,4,5))

#passo 5 - creo rete bayesiana
net = model2network("[O|V:R:L:CL:CH:SE:B:PS][V][R][L][CL][CH][SE][B][PS]")
dfit = custom.fit(net, dist = list(O = cpt_overall, V = cpt_value, R = cpt_rooms, L = cpt_location, CL = cpt_cleanliness, CH = cpt_check_in_front_desk,
                                   SE = cpt_service, B = cpt_business_service, PS = cpt_sentiment))

cat("P(high hemaglobin levels | play water polo and have high hematocrit ratio) =", cpquery(dfit, (O=="4"), (SE == "5" & PS == "4" & CL == "4" & R == "3" & CH == "5" & L == "5" & V == "4" & B == "4")), "\n")
# Analisi
data_test = read.csv("Dataset/hotelTestingSentNO-1.csv", header = TRUE)
data_test$value = as.factor(data_test$value)
data_test$rooms = as.factor(data_test$rooms)
data_test$location = as.factor(data_test$location)
data_test$cleanliness = as.factor(data_test$cleanliness)
data_test$check_in_front_desk = as.factor(data_test$check_in_front_desk)
data_test$service = as.factor(data_test$service)
data_test$business_service = as.factor(data_test$business_service)
data_test$overall = as.factor(data_test$overall)
data_test$sentiment= as.factor(data_test$sentiment)


names(data_test)[names(data_test) == "value"] <- "V"
names(data_test)[names(data_test) == "rooms"] <- "R"
names(data_test)[names(data_test) == "location"] <- "L"
names(data_test)[names(data_test) == "cleanliness"] <- "CL"
names(data_test)[names(data_test) == "check_in_front_desk"] <- "CH"
names(data_test)[names(data_test) == "service"] <- "SE"
names(data_test)[names(data_test) == "business_service"] <- "B"
names(data_test)[names(data_test) == "sentiment"] <- "PS"
names(data_test)[names(data_test) == "overall"] <- "O"



pred = predict(dfit, node="O", data = data_test)

predNum=as.numeric(pred)
qualNum=as.numeric(data_test$O)

#ROC CURVE
areaUc=multiclass.roc(qualNum, predNum)
#risultato roc per tutte e 3 le classi
rs <- areaUc[['rocs']]
#per stampa corretta
plot.roc(rs[[1]],col="blue", asp = NULL, legacy.axes = TRUE, xlab = "1- Specificity")
legend("right", legend = c("Overall1 VS Overall2","Overall1 VS Overall3","Overall1 VS Overall4","Overall1 VS Overall5","Overall2 VS Overall3","Overall2 VS Overall4","Overall2 VS Overall5","Overall3 VS Overall4","Overall3 VS Overall5","Overall4 VS Overall5") , pch = 15, bty = 'n', col = c("blue","red","black","blueviolet","cyan","darkolivegreen","forestgreen","chocolate","deeppink1","goldenrod3"))
lines.roc(rs[[2]], col="red")
lines.roc(rs[[3]], col="black")
lines.roc(rs[[4]], col="blueviolet")
lines.roc(rs[[5]], col="cyan")
lines.roc(rs[[6]], col="darkolivegreen")
lines.roc(rs[[7]], col="forestgreen")
lines.roc(rs[[8]], col="chocolate")
lines.roc(rs[[9]], col="deeppink1")
lines.roc(rs[[10]], col="goldenrod3")
# sapply(2:length(rs),function(z) lines.roc(rs[[z]],col=z ))
text(0.5, 0.63, labels=sprintf("AUC: %0.3f", auc(rs[[1]])), col="blue")
text(0.5, 0.56, labels=sprintf("AUC: %0.3f", auc(rs[[2]])), col="red")
text(0.5, 0.49, labels=sprintf("AUC: %0.3f", auc(rs[[3]])), col="black")
text(0.5, 0.42, labels=sprintf("AUC: %0.3f", auc(rs[[4]])), col="blueviolet")
text(0.5, 0.35, labels=sprintf("AUC: %0.3f", auc(rs[[5]])), col="cyan")
text(0.5, 0.28, labels=sprintf("AUC: %0.3f", auc(rs[[6]])), col="darkolivegreen")
text(0.5, 0.21, labels=sprintf("AUC: %0.3f", auc(rs[[7]])), col="forestgreen")
text(0.5, 0.14, labels=sprintf("AUC: %0.3f", auc(rs[[8]])), col="chocolate")
text(0.5, 0.07, labels=sprintf("AUC: %0.3f", auc(rs[[9]])), col="deeppink1")
text(0.5, 0, labels=sprintf("AUC: %0.3f", auc(rs[[10]])), col="goldenrod3")


confusion.matrix = table(data_test$O, pred)
#variabili utili per calcolo metriche
diag=diag(confusion.matrix)
rowsums = apply(confusion.matrix, 1, sum) 
colsums = apply(confusion.matrix, 2, sum)
p = rowsums / sum(confusion.matrix) 
q = colsums / sum(confusion.matrix) 

#calcolo metriche
acc=sum(diag)/sum(confusion.matrix)
rec = diag / colsums 
prec = diag / rowsums 
f1 = 2 * prec * rec / (prec + rec) 
expAccuracy = sum(p*q)
kappa = (acc - expAccuracy) / (1 - expAccuracy)

aucdf= as.numeric(data.frame(auc(rs[[1]]),auc(rs[[2]]),auc(rs[[3]]),auc(rs[[4]]),auc(rs[[5]]),auc(rs[[6]]),auc(rs[[7]]),auc(rs[[8]]),auc(rs[[9]]),auc(rs[[10]])))
Accuracy=formattable(acc, digits=3, format="f")
Precision=formattable(prec, digits=3, format="f")
Recall=formattable(rec, digits=3, format="f")
Fmeasure=formattable(f1, digits=3, format="f")
AUC=formattable(mean(aucdf), digits=3, format="f")
Kappa=formattable(kappa, digits=3, format="f")

class.metrics=data.frame(Precision, Recall,Fmeasure) 
all.metrics=data.frame(mean(Precision), mean(Recall),mean(Fmeasure),mean(AUC),Accuracy, Kappa)

print("METRICHE PER CLASSE")
print(class.metrics)
print("METRICHE TOTALI")
print(all.metrics)


