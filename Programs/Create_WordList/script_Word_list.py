import csv
import re
from collections import Counter
from nltk.corpus import stopwords

languages=['german','english','italian','spanish','romanian','french','norwegian','dutch','indonesian','portuguese','danish','swedish','hungarian','azerbaijani','finnish','turkish']
stwords = [set(stopwords.words(lan)) for lan in languages]
name="Training"
all_words=[]
file = open("../../Dataset/hotel"+name+".csv", 'r', encoding="utf8")
readerObj = csv.reader(file, delimiter=',', quotechar='"')
# take only position 1-id_author and 3-comment
comments_data=[]
i=1
comments_data = [row[13] for row in readerObj]
del comments_data[0]
fp = open('output/WordList'+name+'Appo.txt', 'w+', encoding="utf8")
fpDetectLanguages = open('output/WordList'+name+'AppoDL.txt', 'w+', encoding="utf8")
for comment in comments_data:
    tokens= comment.split(" ")
    tokens= [re.sub("[^a-zA-Z' ]+","",t).lower()  for t in tokens]
    word_tokens=[]
    for word in tokens:
        present=False
        if("showreview" in word):
            del tokens[tokens.index(word):len(tokens)-1]
        elif len(word) > 1 and word != "" and word.isalpha():
            for stw in stwords:
                if word in stw:
                    present=True
            fpDetectLanguages.write(word+"\n")
            if(present== False):
                fp.write(word+"\n")
    if i%100==0:
        print(i)
    i+=1
wList=[]
fp = open('output/WordList'+name+'Appo.txt', 'r', encoding="utf8")
wList = fp.read().split("\n")
wList=list(set(wList))
wList.sort()
del wList[0]
count = Counter(wList)
fp = open('output/WordList'+name+'.txt', 'w+', encoding="utf8")
i=0
for w in wList:
    fp.write(w)
    if(i<len(wList)-1):
        fp.write("\n")
    i+=1
fp.close()

fpDetectLanguages = open('output/WordList'+name+'AppoDL.txt', 'r', encoding="utf8")
wList = fpDetectLanguages.read().split("\n")
wList=list(set(wList))
wList.sort()
del wList[0]
count = Counter(wList)
fpDetectLanguages = open('output/WordList'+name+'DL.txt', 'w+', encoding="utf8")
i=0
for w in wList:
    fpDetectLanguages.write(w)
    if(i<len(wList)-1):
        fpDetectLanguages.write("\n")
    i+=1
fpDetectLanguages.close()
