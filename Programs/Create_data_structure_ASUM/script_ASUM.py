import csv
import re
import math

name="Training"
def read_sentences_file():
    fw = open("../Create_WordList/output/WordList"+name+".txt", 'r', encoding="utf8")
    words = fw.read().split("\n")
    fs = open("created_files/BagOfSentences"+name+".txt", 'r', encoding="utf8")
    sentences = fs.read().split("\n")
    i = 0
    for sent in sentences:
        if sent!='1':
            print("Document: "+str(i+1))
            for value in sent.split(" "):
                if value!='':
                    print(words[int(value)]+" ",end='')
            print("\n")
            i+=1

# pharse to convert comment in natural language to token language (each number is index of word in WordList.txt
def pharse_comment(sentence):
    tokens = sentence.split(" ")
    sentence = [re.sub("[^a-zA-Z' ]+", "", t).lower() for t in tokens]
    sent=[]
    #next line in BoS.txt
    writed = False
    for word in sentence:
        if("showreview" in word):
            del sentence[sentence.index(word):len(sentence)-1]
        elif len(word) > 1 and word != "" and word.isalpha():
            if word in words:
                writed = True
                sent.append(words[word])
    if writed==True:
        if(len(sent)<=5):
            nl=2
        else:
            nl=5
        fw.write(str(math.ceil(len(sent)/nl))  + "\n")
        div = 1
        for w in sent:
            fw.write(str(w) + " ")  # for each word in sentence
            if (div % nl == 0):
                fw.write("\n")
            div+=1
        if(len(sent)%nl!=0):
            fw.write("\n")
    else:
        fw.write("1\n820\n")

def createDict():
    global words
    words={}
    i=0
    with open("../Create_WordList/output/WordList"+name+".txt", 'r', encoding="utf8") as fwords:
        for word in fwords:
            words[word.strip("\n")]=i
            i+=1

# script that creates "bagOfSentences.txt"
def create_bag_of_sentences(fl):
    global fw
    file = open(fl, 'r', encoding="utf8")
    fw = open("created_files/BagOfSentences"+name+".txt", "w+", encoding="utf8")
    reader = csv.reader(file, delimiter=',', quotechar='"')
    # take only position 1-id_author and 3-comment
    comments_data = [row[13] for row in reader]
    del comments_data[0]
    i=1
    for comment in comments_data:
        pharse_comment(comment)
        print("Documents residuals: "+str(len(comments_data)-i))
        i+=1
    fw.close()

createDict()
create_bag_of_sentences("../../Dataset/hotel"+name+".csv")
# read_sentences_file()
